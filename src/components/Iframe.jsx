import React from 'react';

const Iframe = props => {
  return (
    <div>
      <iframe
        name={props.name}
        src={props.src}
        width={props.width}
        height={props.height}
        >
      </iframe>
    </div>
  );
};

export default Iframe;
