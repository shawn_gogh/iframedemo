import React, { Component } from 'react';
import Button from './components/Button';
import Input from './components/Input';
import Iframe from './components/Iframe';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        query: ""
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleFromSubmit = this.handleFromSubmit.bind(this);
    this._handleListener = this._handleListener.bind(this);
  }


  handleInput (e) {
    let value = e.target.value;
    let name = e.target.name;
    this.setState(
      prevState => ({
        query:value
      }),
      () =>console.log(this.state.query)
    )
  };

  _handleListener (event) {
    const that = this;
    console.log(event.data);
    this.setState(
      prevState => ({
        query:event.data
      }),
      () =>console.log(this.state.query)
    );
  }

  handleFromSubmit (e) {
    e.preventDefault();
    let query = this.state.query;
    const iframe = window.frames.example;
    iframe.postMessage(query,'*');
    window.addEventListener("message", this._handleListener);
  }

  render() {
    return (
      <form className="container-fluid" onSubmit={this.handleFromSubmit}>

        <Input
          inputtype={"text"}
          title={"搜索内容"}
          name={"name"}
          value={this.state.query}
          placeholder={"请输入查询内容"}
          handlechange={this.handleInput}
        />
        <Button
          action={this.handleFromSubmit}
          type={"primary"}
          title={"提交"}
          style={buttonStyle}
        />
        <Iframe
          name={"example"}
          src={kbnSrc}
          width={iframeWidth}
          height={iframeHeight}
        />
      </form>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
const kbnSrc = "http://localhost:5601/kfr/app/kibana#/dashboard/ed8c57e0-f6a3-11e8-ad25-e3aeac3b2e67?_g=()&_a=(description:'',filters:!(),fullScreenMode:!f,options:(darkTheme:!f,hidePanelTitles:!f,useMargins:!t),panels:!((embeddableConfig:(),gridData:(h:15,i:'1',w:24,x:0,y:0),id:'65529c10-907b-11e8-afde-ab951d54f5f1',panelIndex:'1',type:visualization,version:'6.3.1'),(embeddableConfig:(),gridData:(h:15,i:'2',w:24,x:24,y:0),id:'92e44ef0-c076-11e8-8828-d9192e7f6f3d',panelIndex:'2',type:visualization,version:'6.3.1')),query:(language:lucene,query:''),timeRestore:!f,title:ifream%E5%88%86%E4%BA%AB,viewMode:view)";
const iframeWidth = "1500";
const iframeHeight = "800";
export default App;
